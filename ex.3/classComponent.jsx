import React, { Component } from 'react'

export default class ClassComponent extends Component {

  constructor(props)
  {
    super(props);
    this.state  = { c: 1}
  }

  render() {
    return (
      <div>
        <h1>Componente Classe {this.props.value} - {this.state.c}</h1>
        <button onClick={(e) => this.vai(e)}>Vai logo {this.state.c}</button>
      </div>
    )
  }

  vai() {
    var state = { c : this.state.c + 1 }
    this.setState(state);
  }

}
