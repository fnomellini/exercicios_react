import React from 'react'
import ReactDom from 'react-dom'
import NomelliniFamily from './component'

ReactDom.render(
    <NomelliniFamily />,
    document.getElementById('app')
)