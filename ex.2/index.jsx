import React from 'react'
import ReactDom from 'react-dom'
import NomelliniFamily from './component'
import Familia from './familia'
import Member from './member';

ReactDom.render(
    <Familia lastName="Nomellini">
        <Member name="Fernando" />
        <Member name="Tania" />
        <Member name="Stela" />
        <Member name="Marina" />
    </Familia>
    ,
    document.getElementById('app')
)