import React from 'react'
import axios from 'axios'

const ApiRoot = "http://dtmweb:8100/api";
const ClienteApi = ApiRoot + "/Clientes";
const TokenApi = ApiRoot + "/AppAutentication";

function childrenWithProps(children, props) {
    return React.Children.map(children,
        child => React.cloneElement(child, { ...props
        }))
}

function obterEmpresas(USER_TOKEN) {
    const AuthStr = 'Bearer '.concat(USER_TOKEN);
    return new Promise(resolve => {
        axios.get(ClienteApi, { headers: { Authorization: AuthStr } })
        .then(response => {
            resolve(response.data);
         })
        .catch((error) => {
            console.log('error !' + error);
         });
    })
}

function accessToken() {

    return new Promise(resolve => {
        axios.post(TokenApi, {
                user: 'admin'
            })
            .then(function (response) {
                resolve(response.data.accessToken);
            })
            .catch(function (error) {
                console.log(error);
            })
    })
};

export {
    childrenWithProps,
    accessToken,
    obterEmpresas
}