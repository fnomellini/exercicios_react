import axios from 'axios'
import { accessToken, obterEmpresas } from '../utils/reactUtils'
import Empresa from './empresa'
import React, { Component } from 'react'

export default class ClassComponent extends Component {

  constructor(props) {
    super(props);
    this.state = { mensagem: "Clique no botão...", c: 0, token: '-', empresas: [] };
  }


  render() {

    return (

      <div>

          <div className="row">
            <div className="col-md-12">

              <h2>Testes usando React como front end javascript</h2>
              <h3>{this.state.mensagem}</h3>

              <button onClick={(e) => this.vai(e)} className="btn btn-default">Clica ae {this.state.c}</button>
            </div>
          </div>

          <div className="row">

            {
              this.state.empresas.map(function (emp, i) {
                return (
                  <Empresa key={emp.codigo} Codigo={emp.codigo} Nome={emp.nome} />
                );
              })
            }

          </div>

        </div>



    )
  }

  vai() {
    accessToken().then(data => {

      this.setState({
        mensagem: "Aguarde...",
        c: this.state.c + 1,
        empresas: []
      });


      obterEmpresas(data).then(result => {
        this.setState({
          mensagem: "Ok",
          c: this.state.c + 1,
          empresas: result
        })
      })
    });
  }

}
